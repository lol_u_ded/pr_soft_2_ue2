package exercise.jayunit;

import exercise.annotations.BeforeTests;
import exercise.annotations.ExpectException;
import exercise.annotations.MyTest;
import exercise.calculator.CalculatorTest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

public class JayUnit {

	public static void runTests(Class<?> testClass){

		CalculatorTest c = null;
		try {
			c = (CalculatorTest) testClass.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}

		if (c == null) throw new TestClassException("Error while invoking the testClass method!");
		Method[] methods = c.getClass().getDeclaredMethods();

		//checks Parameters, Return-Types and if there is more than 1 @BeforeTest
		checkMethods(methods);

		//Crop the BeforeTest method.
		Optional<Method> init =Arrays.stream(methods)
				.filter(a -> a.isAnnotationPresent(BeforeTests.class))
				.findFirst();

		//Get all methods that should run
		Method[] myTests = Arrays.stream(methods)
				.filter(s -> s.isAnnotationPresent(MyTest.class))
				.filter(s -> !s.getAnnotation(MyTest.class).ignore())
				.filter(s -> !s.isAnnotationPresent(BeforeTests.class))
				.toArray(Method[]::new);


		for (Method myTest : myTests) {

			//BeforeTest
			if (init.isPresent()) {
				try {
					init.get().invoke(c);
				} catch (IllegalAccessException | InvocationTargetException e) {
					throw new TestClassException("Error while invoking the BeforeTest method!");
				}
			}
			// check the test
			boolean failed = false;
			// Case with ExpectedException
			if (myTest.isAnnotationPresent(ExpectException.class)) {
				try {
					myTest.invoke(c);
				} catch (InvocationTargetException ite) {
					Class<? extends Exception> ex = myTest.getAnnotation(ExpectException.class).exception();
					failed = !ite.getCause().getClass().equals(ex);
				} catch (IllegalAccessException e){
					throw new TestClassException("Error while invoking the MyTest method!");
				}
			// Case only with MyTest
			} else {
				try {
					myTest.invoke(c);
				} catch (InvocationTargetException ite) {
					try {
						throw ite.getCause();
					} catch (TestFailedException e) {
						failed = true;
					} catch (Throwable throwable) {
						throw new TestClassException("Other error while invoking the MyTest method!");
					}
				} catch (IllegalAccessException e) {
					throw new TestClassException("Error while invoking the MyTest method!");
				}
			}
			if (failed) {
				System.out.println(myTest.getName() + " failed.");
			} else {
				System.out.println(myTest.getName() + " OK.");
			}
		}
	}

	private static void checkMethods(Method[] methods) {
		if (methods == null) {
			throw new TestClassException("Method Array is null!");
		}
		int counter = 0;
		for (Method m : methods) {
			if (m.getReturnType().getName().equals("void")) {
				if (m.getParameters().length != 0) {
					throw new TestClassException("More than one Parameter in " + m.getName() + "!");
				}
			} else {
				throw new TestClassException("Methods must have return type void!");
			}
			if (m.isAnnotationPresent(BeforeTests.class)) {
				counter++;
			}
		}
		if(counter > 1){
			throw new TestClassException("More than one @BeforeTest Annotation present!");
		}
	}
}