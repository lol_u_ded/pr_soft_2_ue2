package exercise.main;

import exercise.calculator.CalculatorTest;
import exercise.jayunit.JayUnit;

public class Runner {
	public static void main(String[] args){
		Class<?> tests = CalculatorTest.class;
		JayUnit.runTests(tests);
	}
}
