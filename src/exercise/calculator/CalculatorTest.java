package exercise.calculator;

import exercise.annotations.BeforeTests;
import exercise.annotations.ExpectException;
import exercise.annotations.MyTest;
import exercise.jayunit.TestClassException;
import exercise.jayunit.TestFailedException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

public class CalculatorTest {

	private Calculator sut;    // system under test

	@BeforeTests
	public void init() {
		sut = new Calculator();
	}

	@MyTest
	@ExpectException(exception = ArithmeticException.class)
	public void testDivideZero() throws TestFailedException, TestClassException {
		try {
			sut.divide(0);
		} catch (ArithmeticException e) {
			throw e;
		} catch (Exception e) {
			throw new TestClassException("Problem while executing method testDivideZero!");
		}
		// If there was no Error while dividing with zero the test basically failed
		throw new TestFailedException();
	}

	@MyTest
	public void testNegativeAdd() throws TestFailedException, TestClassException {
		try {
			sut.add(-1);
			Class<?> calculatorClass = sut.getClass();
			Field stateField = calculatorClass.getDeclaredField("state");
			stateField.setAccessible(true);
			int state = (int) stateField.get(sut);
			if(state != -1){
				throw new TestFailedException();
			}
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new TestClassException("Error accessing Fields in method testNegativeAdd");
		}
	}

	@MyTest
	public void testResetRem() throws TestFailedException, TestClassException {
		int n = new Random().nextInt() % 1000;
		try {
			Class<?> calculatorClass = sut.getClass();
			Field stateField = calculatorClass.getDeclaredField("rem");
			stateField.setAccessible(true);
			stateField.set(sut, n);
			Method resetRem = calculatorClass.getDeclaredMethod("resetRem");
			resetRem.setAccessible(true);
			resetRem.invoke(sut);
			int rem = (int) stateField.get(sut);
			if (rem != 0) {
				throw new TestFailedException();
			}
		} catch (NoSuchFieldException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
			throw new TestClassException("Error accessing Fields in method testResetRem!");
		}
	}

	@MyTest(ignore = true)
	public void dummyTest() {
		//  empty test, should be ignored by JayUnit
	}
}
