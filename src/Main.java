import exercise.annotations.BeforeTests;
import exercise.annotations.MyTest;
import exercise.calculator.Calculator;
import exercise.calculator.CalculatorTest;
import org.junit.jupiter.api.Assertions;

import java.lang.reflect.*;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Class<?> c1 = int.class; // all other primitive Types are possible as well

        System.out.println(c1.getName());

        Class<?> dogClass = Dog.class;

        System.out.println("Simple Name: " + dogClass.getSimpleName());
        System.out.println("Name: " + dogClass.getName());
        System.out.println("Is an Interface: " + dogClass.isInterface());
        System.out.println("Is an Array: " + dogClass.isArray());
        System.out.println("---------------");

        Constructor<?>[] constructors = dogClass.getConstructors();
        System.out.println("List of constructors in " + dogClass.getName() + ": ");
        Arrays.stream(constructors).sequential().forEach(System.out::println);
        System.out.println("---------------");

        for(Constructor<?> c : constructors){
            System.out.println("Constructor name:" + c.getName());
            System.out.println("--params--");
            if(c.getParameterCount() == 0 ){
                System.out.println("No arg constructor.");
            }else{
                Parameter[] params = c.getParameters();
                Arrays.stream(params).sequential().forEach(s -> System.out.println(s.getName() + ": " + s.getType()));
            }
        }
        System.out.println("---------------");

//        Method[] meth = dogClass.getMethods();              // returns public Methods
        Method[] meth = dogClass.getDeclaredMethods();      // returns all Methods
        System.out.println("Methods of " + dogClass.getSimpleName() + ": ");
        Arrays.stream(meth).sequential()
                .forEach(s -> System.out.println(Modifier.toString(s.getModifiers()) + ", " + s.getName() + ": " + s.getReturnType().getName() + ", params: " + s.getParameterCount()));
        System.out.println("---------------");


        System.out.println("get Fields");
        Field[] f = dogClass.getDeclaredFields();       // access all fields even private ones
        Arrays.stream(f).sequential().forEach(s -> System.out.println(Modifier.toString(s.getModifiers()) + ", " + s.getType().getName() + ", " +s.getName() ));
        System.out.println("---------------");
        System.out.println("Homework");
        System.out.println("---------------");


        System.out.println("Access private int field");
        Calculator sut = new Calculator();
        Class<?> calculatorClass = sut.getClass();
        Field stateField = calculatorClass.getDeclaredField("state");
        stateField.setAccessible(true);
        int state = (int) stateField.get(sut);
        System.out.println("int directly from private field: "+state);
        System.out.println("using printer: ");
        sut.printState();

        sut.add(1);
        System.out.println("Adding 1: ");
        System.out.println("int directly from private field: "+state);
        System.out.println("using printer: ");
        sut.printState();

        System.out.println("recreate and adding 1 ");
        sut = new Calculator();
        sut.add(1);
        calculatorClass = sut.getClass();
        stateField = calculatorClass.getDeclaredField("state");
        stateField.setAccessible(true);
        state = (int) stateField.get(sut);

        System.out.println("int directly from private field: "+state);
        System.out.println("using printer: ");
        sut.printState();
        System.out.println("---------------");

        sut = new Calculator();
        Calculator finalSut = sut;
        Assertions.assertThrows(ArithmeticException.class, ()-> finalSut.divide(0));
        System.out.println(" Testing Arithmetic stuff x/0");
        System.out.println("...done");

        System.out.println("---------------");

        System.out.println("-------- Testing my shit -------");
        Class<?> tests = CalculatorTest.class;
//		System.out.println("Class Tests: ");
//		System.out.println();
        Method[] methods = tests.getDeclaredMethods();
        for (Method method : methods){
            System.out.println("Name: " + method.getName());
            System.out.println("ReturnType: " + method.getReturnType());
            System.out.println("AnnotatedReturnType: " + method.getAnnotatedReturnType());
            System.out.println("Modifiers: " + method.getModifiers());
            System.out.println("isAnnotationPresent(BeforeTest)" + method.isAnnotationPresent(BeforeTests.class));
            System.out.println("isAnnotationPresent(MyTest)" + method.isAnnotationPresent(MyTest.class));
            if(method.isAnnotationPresent(MyTest.class)){
            }
            System.out.println();
        }

        System.out.println("---------------");












    }
}
