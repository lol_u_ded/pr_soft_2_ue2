package demo.annotation;

public interface Parser<T>{
	T parse(String text);
}
