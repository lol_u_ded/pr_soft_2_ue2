package demo.annotation;

import demo.person.Gender;
import demo.person.Person;

public class PersonParser implements Parser<Person>{

	@Override
	public Person parse(String text) {
		// parses: Max Male
		String[] ws = text.split(" ");
		return new Person(ws[0], ws[1].equals("Male") ? Gender.Male : Gender.Female);
	}

}
