package demo.demo;

import java.lang.reflect.InvocationTargetException;

import demo.annotation.Parse;
import demo.annotation.Parser;
import demo.person.Person;

public class Demo3 {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Class<Person> p = Person.class;
		
		if(p.isAnnotationPresent(Parse.class)) {
			Parse pAnn = p.getAnnotation(Parse.class);
			Class<? extends Parser> parserCl = pAnn.value();
			
			Object o = parserCl.getConstructor().newInstance().parse("Hans Male");
			System.out.println(o);
		}
	}

}
